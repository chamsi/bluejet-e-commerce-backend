package com.industry.frontend.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.industry.frontend.security.oauth2.user.OAuth2UserInfo;
import com.industry.frontend.security.oauth2.user.OAuth2UserInfoFactory;
import com.Industry.common.entity.Role;
import com.Industry.common.entity.User;
import com.Industry.common.exception.UsernameAlreadyUsedException;
import com.industry.frontend.dto.LocalUser;
import com.industry.frontend.dto.SignUpRequest;
import com.industry.frontend.dto.SocialProvider;
import com.industry.frontend.exception.OAuth2AuthenticationProcessingException;
import com.industry.frontend.exception.UserAlreadyExistAuthenticationException;

import com.industry.frontend.repo.RoleRepository;
import com.industry.frontend.repo.UserRepository;
import com.industry.frontend.util.GeneralUtils;



@Service
public class UserServiceImpl implements UserService {
 
    @Autowired
    private UserRepository userRepository;
 
    @Autowired
    private RoleRepository roleRepository;
 
    @Autowired
    private PasswordEncoder passwordEncoder;
    
	 @Autowired
	 private SimpMessagingTemplate template;
	 
	 @Autowired
     UserService userService;

    @Override
    @Transactional(value = "transactionManager")
    public User registerNewUser(final SignUpRequest signUpRequest) throws UserAlreadyExistAuthenticationException {
        if (signUpRequest.getUserID() != null && userRepository.existsById(signUpRequest.getUserID())) {
            throw new UserAlreadyExistAuthenticationException("User with User id " + signUpRequest.getUserID() + " already exist");
        } else if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new UserAlreadyExistAuthenticationException("User with email id " + signUpRequest.getEmail() + " already exist");
        }
        User user = buildUser(signUpRequest);
        Date now = Calendar.getInstance().getTime();
        user.setUsername(user.getEmail());
        user.setCreatedDate(now);
        user.setModifiedDate(now);
        user = userRepository.save(user);
        userRepository.flush();
        return user;
    }
 
    private User buildUser(final SignUpRequest formDTO) {
        User user = new User();
        String[] Names = formDTO.getFullName().split(" ");
        user.setFirstName(Names[0].toString());
        user.setLastName(Names[1].toString());
        user.setEmail(formDTO.getEmail());
        user.setPassword(passwordEncoder.encode(formDTO.getPassword()));
        final HashSet<Role> roles = new HashSet<Role>();
        roles.add(roleRepository.findByName("Customer"));
        user.setRoles(roles);
        user.setProvider(formDTO.getSocialProvider().getProviderType());
        user.setEnabled(true);
        user.setProviderUserId(formDTO.getProviderUserId());
        return user;
    }
 
    @Override
    public User findUserByEmail(final String email) {
        return userRepository.findByEmail(email);
    }
 
    @SuppressWarnings("deprecation")
	@Override
    @Transactional
    public LocalUser processUserRegistration(String registrationId, Map<String, Object> attributes, OidcIdToken idToken, OidcUserInfo userInfo) {
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(registrationId, attributes);
        if (StringUtils.isEmpty(oAuth2UserInfo.getName())) {
            throw new OAuth2AuthenticationProcessingException("Name not found from OAuth2 provider");
        } else if (StringUtils.isEmpty(oAuth2UserInfo.getEmail())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }
        SignUpRequest userDetails = toUserRegistrationObject(registrationId, oAuth2UserInfo);
        User user = findUserByEmail(oAuth2UserInfo.getEmail());
        if (user != null) {
            if (!user.getProvider().equals(registrationId) && !user.getProvider().equals(SocialProvider.LOCAL.getProviderType())) {
                throw new OAuth2AuthenticationProcessingException(
                        "Looks like you're signed up with " + user.getProvider() + " account. Please use your " + user.getProvider() + " account to login.");
            }
            user = updateExistingUser(user, oAuth2UserInfo);
            user.setUsername(user.getEmail());
            try {
	            User connectedUser = userService.connect(user);
	            template.convertAndSend("/channel/login", connectedUser);
	        } catch (UsernameAlreadyUsedException e) {}
        } else {
            user = registerNewUser(userDetails);
        }
 
        return LocalUser.create(user, attributes, idToken, userInfo);
    }
 
    private User updateExistingUser(User existingUser, OAuth2UserInfo oAuth2UserInfo) {
        existingUser.setFirstName(oAuth2UserInfo.getName());
        return userRepository.save(existingUser);
    }
 
    private SignUpRequest toUserRegistrationObject(String registrationId, OAuth2UserInfo oAuth2UserInfo) {
        return SignUpRequest.getBuilder().addProviderUserID(oAuth2UserInfo.getId()).addDisplayName(oAuth2UserInfo.getName()).addEmail(oAuth2UserInfo.getEmail())
                .addSocialProvider(GeneralUtils.toSocialProvider(registrationId)).addPassword("changeit").build();
    }
 
    @Override
    public Optional<User> findUserById(Long id) {
        return userRepository.findById(id);
    }
    
    
    public User connect(User user) throws UsernameAlreadyUsedException {
        User dbUser = userRepository.findByUsername(user.getUsername());

        if (dbUser != null) {

            if (dbUser.getConnected()) {
                throw new UsernameAlreadyUsedException("This user is already connected: " + dbUser.getUsername());
            }

            dbUser.setConnected(true);
            return userRepository.save(dbUser);
        }

        user.setConnected(true);
        return userRepository.save(user);
    }

   
    public User disconnect(User user) {
        if (user == null) {
            return null;
        }

        User dbUser = userRepository.findByUsername(user.getUsername());
        if (dbUser == null) {
            return user;
        }

        dbUser.setConnected(false);
        return userRepository.save(dbUser);
    }
    
    
    
}
