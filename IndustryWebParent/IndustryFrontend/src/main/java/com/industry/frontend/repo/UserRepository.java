package com.industry.frontend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.Industry.common.entity.User;




@Repository
public interface UserRepository  extends JpaRepository<User, Long> {

	User findByEmail(String email);
	 
    boolean existsByEmail(String email);
    
    User findByUsername(String username);

    @Transactional
    Long deleteByUsername(String username);
	
}
