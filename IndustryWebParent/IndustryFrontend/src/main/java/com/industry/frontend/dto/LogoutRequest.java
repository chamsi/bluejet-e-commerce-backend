package com.industry.frontend.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class LogoutRequest {
    @NotBlank
    private String email;
}
