package com.industry.frontend.service;

import java.util.Map;
import java.util.Optional;

import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;

import com.Industry.common.entity.User;
import com.Industry.common.exception.UsernameAlreadyUsedException;
import com.industry.frontend.dto.LocalUser;
import com.industry.frontend.dto.SignUpRequest;
import com.industry.frontend.exception.UserAlreadyExistAuthenticationException;




public interface UserService {

	 public User registerNewUser(SignUpRequest signUpRequest) throws UserAlreadyExistAuthenticationException;
	 
	    User findUserByEmail(String email);
	 
	    Optional<User> findUserById(Long id);
	 
	    LocalUser processUserRegistration(String registrationId, Map<String, Object> attributes, OidcIdToken idToken, OidcUserInfo userInfo);
        
	    User connect(User user) throws UsernameAlreadyUsedException;

	    User disconnect(User user);




}

	
	

