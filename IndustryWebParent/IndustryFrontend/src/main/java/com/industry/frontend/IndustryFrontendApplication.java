package com.industry.frontend;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;



@SpringBootApplication(scanBasePackages = "com.industry.frontend")
@EntityScan({"com.Industry.common.entity"})
@EnableJpaRepositories
@EnableTransactionManagement
public class IndustryFrontendApplication extends SpringBootServletInitializer  {

	public static void main(String[] args) {
		SpringApplicationBuilder app = new SpringApplicationBuilder(IndustryFrontendApplication.class);
		app.run();
	}

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(IndustryFrontendApplication.class);
	}


}
