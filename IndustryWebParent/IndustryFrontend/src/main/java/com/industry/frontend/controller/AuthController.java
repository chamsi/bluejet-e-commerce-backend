package com.industry.frontend.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.Industry.common.entity.User;
import com.Industry.common.exception.UsernameAlreadyUsedException;
import com.industry.frontend.dto.ApiResponse;
import com.industry.frontend.dto.JwtAuthenticationResponse;
import com.industry.frontend.dto.LocalUser;
import com.industry.frontend.dto.LoginRequest;
import com.industry.frontend.dto.LogoutRequest;
import com.industry.frontend.dto.SignUpRequest;
import com.industry.frontend.exception.UserAlreadyExistAuthenticationException;
import com.industry.frontend.repo.UserRepository;
import com.industry.frontend.security.jwt.TokenProvider;
import com.industry.frontend.service.UserService;
import com.industry.frontend.util.GeneralUtils;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserService userService;

	@Autowired
	TokenProvider tokenProvider;
	
	 @Autowired
	 private UserRepository userDao;
	
	 @Autowired
	 private SimpMessagingTemplate template;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);	
		String jwt = tokenProvider.createToken(authentication);
		LocalUser localUser = (LocalUser) authentication.getPrincipal();
		localUser.getUser().setUsername(localUser.getUser().getEmail());
	
		try {
            User connectedUser = userService.connect(localUser.getUser());
            template.convertAndSend("/channel/login", connectedUser);
        } catch (UsernameAlreadyUsedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, GeneralUtils.buildUserInfo(localUser)));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		try {
			userService.registerNewUser(signUpRequest);
		} catch (UserAlreadyExistAuthenticationException e) {
			log.error("Exception Ocurred", e);
			return new ResponseEntity<>(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok().body(new ApiResponse(true, "User registered successfully"));
	}
	
	    @RequestMapping(value = "/logout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(HttpStatus.NO_CONTENT)
	    public ResponseEntity<?> logout( @Valid @RequestBody LogoutRequest logoutRequest) {
	    	User user =userDao.findByUsername(logoutRequest.getEmail());
	        User disconnectedUser = userService.disconnect(user);
	        template.convertAndSend("/channel/logout", disconnectedUser);
			return ResponseEntity.ok().body(new ApiResponse(true, "User logout successfully"));
	    }

	  

	    @RequestMapping(value = "/clearAll", method = RequestMethod.GET)
	    @ResponseStatus(HttpStatus.NO_CONTENT)
	    public void clearAll() {
	        userDao.deleteAll();
	    }
	
}