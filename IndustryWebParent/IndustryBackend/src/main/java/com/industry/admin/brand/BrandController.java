package com.industry.admin.brand;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.Industry.common.entity.Brand;
import com.Industry.common.entity.Category;
import com.Industry.common.exception.BrandNotFoundException;
import com.industry.admin.FileUploadUtil;
import com.industry.admin.category.CategoryService;




@Controller
public class BrandController {

		@Autowired
		private BrandService brandservice;
		@Autowired
		private CategoryService catservice;
		

@GetMapping("/brands")
public String listFirstPage(Model model) {
	return listByPage(1,model,"name","asc",null); 
}
@GetMapping("/brands/new")
public String newBrand(Model model) {
	List<Category> listCategories = catservice.listCategoriesUsedInForm();
	
	model.addAttribute("brand", new Brand());
	model.addAttribute("listCategories", listCategories);
	model.addAttribute("pageTitle", "Create New Brand");
	return"brands/brand_form";	
}


@PostMapping("/brands/save")
public String saveBrand(Brand brand, @RequestParam("fileImage") MultipartFile multipartFile,
		RedirectAttributes ra) throws IOException {
	if (!multipartFile.isEmpty()) {
		
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		brand.setLogo(fileName);
		Brand savedBrand = brandservice.save(brand);
		String uploadDir = "../brand-logos/" + savedBrand.getId();
		
		

		FileUploadUtil.cleanDir(uploadDir);
		FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

	} else {
		brandservice.save(brand);
	}

	ra.addFlashAttribute("message", "The brand has been saved successfully.");
	return "redirect:/brands";	
}


@GetMapping("/brands/edit/{id}")
public String editBrand(@PathVariable(name = "id") Integer id, Model model,
		RedirectAttributes ra) {
	try {

		List<Category> listCategories = catservice.listCategoriesUsedInForm();
		Brand brand = brandservice.get(id);
		model.addAttribute("listCategories", listCategories);
		model.addAttribute("brand", brand);
		model.addAttribute("pageTitle", "Edit Brand (ID: " + id + ")");

		return "brands/brand_form";			
	} catch (BrandNotFoundException ex) {
		ra.addFlashAttribute("message", ex.getMessage());
		return "redirect:/brands";
	}
}

@GetMapping("/brands/delete/{id}")
public String deleteBrand(@PathVariable(name = "id") Integer id, 
		Model model,
		RedirectAttributes redirectAttributes) {
	try {
		brandservice.delete(id);
		String brandDir = "../brand-logos/" + id;
		FileUploadUtil.removeDir(brandDir);
		redirectAttributes.addFlashAttribute("messageSuccess", 
				"The brand ID " + id + " has been deleted successfully");
	} catch (BrandNotFoundException ex) {
		redirectAttributes.addFlashAttribute("message", ex.getMessage());
	}

	return "redirect:/brands";
}	

@GetMapping("/brands/page/{pageNum}")
public String listByPage(
		@PathVariable(name = "pageNum") int pageNum,Model model,
		@Param("sortField") String sortField,
		@Param("sortDir") String sortDir,
		@Param("keyword") String keyword) {
	

	
	Page<Brand> page=brandservice.listByPage(pageNum ,sortField,sortDir,keyword);
	List<Brand> listBrands = page.getContent();
	
	System.out.println("Pagenum =" +pageNum);
	System.out.println("Total elements = "+page.getTotalElements());
	System.out.println("Total pages = "+page.getTotalPages());
    long startCount =(pageNum -1)* brandservice.BRANDS_PER_PAGE+1;
    long endCount =startCount + brandservice.BRANDS_PER_PAGE-1;
	if(endCount> page.getTotalElements()) {
		endCount =page.getTotalElements();
	}
	int totalPages=page.getTotalPages();
	String reverseSortDir =sortDir.equals("asc")?"desc":"asc";
	model.addAttribute("currentPage",pageNum);
	model.addAttribute("moduleURL","/brands");
	model.addAttribute("totalPages",totalPages);
	model.addAttribute("startCount",startCount);
	model.addAttribute("endCount",endCount);
	model.addAttribute("totalItems",page.getTotalElements());
	model.addAttribute("listBrands",listBrands);
	model.addAttribute("sortField",sortField);
	model.addAttribute("sortDir",sortDir);
	model.addAttribute("reverseSortDir",reverseSortDir);
	model.addAttribute("keyword",keyword);
	System.out.println("startcount =" +startCount);
	System.out.println("endCount =" +endCount);
	return "brands/brands"; 	
} 




}
