package com.industry.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan({"com.Industry.common.entity","com.Industry.admin.user"})
public class IndustryBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndustryBackendApplication.class, args);
	}

}
