package com.industry.admin.user;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.Industry.common.entity.Role;



@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
@Rollback(false)
public class RoleRepositoryTests {
@Autowired
private RoleRepository repo;
@Test
public void testCreateFirstRole() {
	
	Role customer = new Role("Customer", "he only see products place orders buy products ");
	Role savedRole = repo.save(customer);
	
	/*The assertThat is one of the JUnit methods from the Assert object that can be used to check if a specific value match to an expected one.*/
	assertThat(savedRole.getId()).isGreaterThan(0);
}/*
@Test
public void testCreateRestRoles() {
	
	Role roleSalesperson = new Role("Salesperson", "manage product price, "
			+ "customers, shipping, orders and sales report");
	Role customer = new Role("Customer", "he only see products place orders buy products ");
	
	Role roleEditor = new Role("Editor", "manage categories, brands, "
			+ "products, articles and menus");
	
	Role roleShipper = new Role("Shipper", "view products, view orders "
			+ "and update order status");
	
	Role roleAssistant = new Role("Assistant", "manage questions and reviews");
	
	repo.saveAll(List.of(roleSalesperson, roleEditor, roleShipper, roleAssistant,customer));
}*/
}
