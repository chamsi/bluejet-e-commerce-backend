package com.industry.admin.user;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.Rollback;

import com.Industry.common.entity.Role;
import com.Industry.common.entity.User;

import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;

@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {

	private UserRepository repo;
	// provided by spring jpa for uni testing
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	public UserRepositoryTests(UserRepository repo, TestEntityManager entityManager) {
		super();
		this.repo = repo;
		this.entityManager = entityManager;
	}
	
	@Test
	public void testCreateNewUserWithOneRole() {
    	Role roleAdmin =entityManager.find(Role.class, 1);
		User userNameHM =new User("chamsjlassi2@gmail.com","$2a$10$lXcVpbziKnNK/J.VVZztP.5bHYpCceNkbbKLyjrmTdqJn.5Ba6ugy","jlassi","chams");
		userNameHM.addRole(roleAdmin);
		User savedUser = repo.save(userNameHM);
		assertThat(savedUser.getId()).isGreaterThan(0);
		
	}/*
	@Test
	public void testCreateNewUserWithTwoRoles() {
		User userAhemd =new User("ahmed@gmail.com","pass2012","jlassi","ahmed");
		Role roleEditor = new Role(3);
		Role roleAssistant = new Role(5);
		
		userAhemd.addRole(roleEditor);
		userAhemd.addRole(roleAssistant);
		User savedUser = repo.save(userAhemd);
		assertThat(savedUser.getId()).isGreaterThan(0);
		
	}/*
	@Test
	public void testListAllUsers() {
		Iterable<User> listUsers = repo.findAll();
		listUsers.forEach(user -> System.out.println(user));		
	
	}
	@Test
	public void testGetUserById() {
		User userNam = repo.findById(1).get();
        System.out.println(userNam);
        assertThat(userNam).isNotNull();
	}/*
	@Test
	public void testUpdateUserDetails() {
		User userNam = repo.findById(1).get();
		userNam.setEmail("chamsjlassi1@gmail.com");
        userNam.setEnabled(true);
        repo.save(userNam);
		System.out.println(userNam);
       
	}*/
	/*
	@Test
	public void testUpdateUserRoles() {
		
		User userAhemd = repo.findById(2).get();
		Role roleSalesperson = new Role(2);
		userAhemd.getRoles().remove(roleSalesperson);		
        repo.save(userAhemd);
  
	}*//*
	@Test
	public void testDeleteUser() {
		Integer userId = 2 ;
		repo.deleteById(userId);
	}*//*
	
	@Test
	public void testGetUserByEmail() {
		String email = "chamsjlassi1@gmail.com";
		User user =repo.getUserByEmail(email);
		assertThat(user).isNotNull();
		
	}*/
	/*
	@Test
	public void testCountById() {
		Integer id = 1;
		Long countById =repo.countById(id);
		assertThat(countById).isNotNull().isGreaterThan(0);
		
	}*//*
	
	@Test
	public void testDisableUser() {
		Integer id = 1;
		repo.updateEnableStatus(id, false);
		
	}*/
	@Test
	public void testEnableUser() {
		Long id = (long) 1;
		repo.updateEnableStatus(id, true);
		
	}
	@Test
	public void testListFirstPage() {
	int pageNumber =1;
	int pageSize =4;

	Pageable pageable = PageRequest.of(pageNumber, pageSize);
	Page<User> page = repo.findAll(pageable);

	List<User> listUsers = page.getContent();

	listUsers.forEach(user -> System.out.println(user));

	assertThat(listUsers.size()).isEqualTo(pageSize);
	}


}
